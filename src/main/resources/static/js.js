//var client=null;

function showMessage(value){
    //tworze nowy paragraf
    var newResponse = document.createElement('p');
    //do tego paragrafu wrzucam nowa wartosc tekstowa
    newResponse.appendChild(document.createTextNode(value));
    //wyciagam z mojego dokumentu respons (index.html)
    var response =document.getElementById('response');
    //doklejam moja nowa wadomosc
    response.appendChild(newResponse);
}

function connect(){
    client = Stomp.client('ws://localhost:8080/chat');
    client.connect({}, function(frame){
        client.subscribe("/topic/messages",function(message){
                showMessage(JSON.parse(message.body).value)
            });

        })
    }

    function showMessage(){
        var messageToSend = document.getElementById('messageToSend').value;
        client.send("/app/chat",{}, JSON.stringify({'value': messageToSend}));
    }