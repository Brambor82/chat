package pl.baranski.chat;

public class ChatMessage{

    //klasa modelowa posiada tylko value

    //komunikat wymieniany miedzy modelem a klientem
    private String value;

    public String getValue(){
        return value;
    }

    public void setValue(String value){
        this.value=value;
    }

    public ChatMessage(String value){
        this.value=value;
    }

    public ChatMessage(){
    }
}
